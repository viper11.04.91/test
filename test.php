<?php
if(!empty($arFolder)){
	$arFilter = [
		'IBLOCK_CODE' => 'CATALOG_SECTION_DATA',
		'%PROPERTY_URL_PAGE' => $arFolder,
		'!CODE' => 'page'
	];
	$arOrder = ['PROPERTY_URL_PAGE' => 'ASC'];
	$arSelect = [
		'ID',
		'NAME',
		'CODE',
		'PREVIEW_TEXT',
		'DETAIL_TEXT',
		'PREVIEW_PICTURE',
		'DETAIL_PICTURE',
		'PROPERTY_URL_PAGE',
		'PROPERTY_URL_BANNER',
		'PROPERTY_SECTION_NAME',
		'PROPERTY_CONSTRUCTOR_NAME',
		'PROPERTY_ID_BRAND',
		'PROPERTY_SEO_TITLE',
		'PROPERTY_SEO_H1',
		'PROPERTY_SEO_DESCRIPTION'
	];
	$el = new CIBlockElement();
	$rs = $el->GetList($arOrder, $arFilter, false, false, $arSelect);
	while($item = $rs->fetch()) {
		if ($item["CODE"] === "page-tab") {
			$arPageTab[$item["CODE"]][$item["ID"]] = $item;
		} else {
			$arOther[] = $item;
		}
	}
}